package com.raul.sqlitedemoandroid.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.raul.sqlitedemoandroid.database.model.Student;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "students_db";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // create students table
        db.execSQL(Student.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Student.TABLE_NAME);
        // Create tables again
        onCreate(db);
    }

    public long insertStudentRec(String stdName,String stdAddress,String stdPhoneNumber,String stdRollNo) {

        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        //Content values
        ContentValues values = new ContentValues();

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(Student.COLUMN_STUDENT_NAME, stdName);
        values.put(Student.COLUMN_STUDENT_ADDRESS, stdAddress);
        values.put(Student.COLUMN_STUDENT_PHONE, stdPhoneNumber);
        values.put(Student.COLUMN_STUDENT_ROLL, stdRollNo);

        long id=db.insert(Student.TABLE_NAME,null,values);

        db.close();

        return id;

    }

    public Student getStudent(long id) {

        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor=db.query(Student.TABLE_NAME,
                            new String[]{Student.COLUMN_ID,Student.COLUMN_STUDENT_NAME,Student.COLUMN_STUDENT_ADDRESS,Student.COLUMN_STUDENT_PHONE,Student.COLUMN_STUDENT_ROLL,Student.COLUMN_TIMESTAMP},
                            Student.COLUMN_ID+"=?",
                            new String[]{String.valueOf(id)},
                            null,null,null,null);

        if (cursor != null)
            cursor.moveToFirst();

        Student student=new Student(
                cursor.getInt(cursor.getColumnIndex(Student.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_NAME)),
                cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_ADDRESS)),
                cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_PHONE)),
                cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_ROLL)),
                cursor.getString(cursor.getColumnIndex(Student.COLUMN_TIMESTAMP)));

        cursor.close();
        return student;

    }

   public List<Student> getAllStudents() {

        List<Student> students=new ArrayList<>();

       // Select All Query
       String selectQuery = "SELECT  * FROM " + Student.TABLE_NAME +
                            " ORDER BY " + Student.COLUMN_TIMESTAMP + " DESC";

       SQLiteDatabase db = this.getWritableDatabase();
       Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
        do{
            Student student=new Student();
            student.setId(cursor.getInt(cursor.getColumnIndex(Student.COLUMN_ID)));
            student.setStudentName(cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_NAME)));
            student.setStudentAddress(cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_ADDRESS)));
            student.setStudentPhone(cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_PHONE)));
            student.setStudentRoll(cursor.getString(cursor.getColumnIndex(Student.COLUMN_STUDENT_ROLL)));

            students.add(student);

        } while (cursor.moveToNext());
        }


        db.close();

        return students;
   }

    public int getStudentsCount() {

        String countQuery = "SELECT  * FROM " + Student.TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        return count;

    }

    public int updateStudentRec(Student student) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(Student.COLUMN_STUDENT_NAME, student.getStudentName());
        values.put(Student.COLUMN_STUDENT_ADDRESS, student.getStudentAddress());
        values.put(Student.COLUMN_STUDENT_PHONE, student.getStudentPhone());
        values.put(Student.COLUMN_STUDENT_ROLL, student.getStudentRoll());

        // updating row
        return db.update(Student.TABLE_NAME, values, Student.COLUMN_ID + " = ?",
                new String[]{String.valueOf(student.getId())});

    }

    public void deleteNote(Student student) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(student.TABLE_NAME, student.COLUMN_ID + " = ?",
                new String[]{String.valueOf(student.getId())});
        db.close();
    }


}
