package com.raul.sqlitedemoandroid.database.model;

public class Student {

    public static final String TABLE_NAME = "student";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_STUDENT_NAME="name";
    public static final String COLUMN_STUDENT_ADDRESS="address";
    public static final String COLUMN_STUDENT_PHONE="phone";
    public static final String COLUMN_STUDENT_ROLL="roll";
    public static final String COLUMN_TIMESTAMP = "timestamp";

    private int id;
    private String studentName;
    private String studentAddress;
    private String studentPhone;
    private String studentRoll;
    private String timestamp;

    //Create table SQL query
    public static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + COLUMN_STUDENT_NAME + " TEXT,"
            + COLUMN_STUDENT_ADDRESS + " TEXT,"
            + COLUMN_STUDENT_PHONE + " TEXT,"
            + COLUMN_STUDENT_ROLL + " TEXT,"
            + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    public Student() {

    }

    public Student(int id, String studentName, String studentAddress, String studentPhone, String studentRoll, String timestamp) {
        this.id = id;
        this.studentName = studentName;
        this.studentAddress = studentAddress;
        this.studentPhone = studentPhone;
        this.studentRoll = studentRoll;
        this.timestamp = timestamp;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentAddress() {
        return studentAddress;
    }

    public void setStudentAddress(String studentAddress) {
        this.studentAddress = studentAddress;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getStudentRoll() {
        return studentRoll;
    }

    public void setStudentRoll(String studentRoll) {
        this.studentRoll = studentRoll;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
