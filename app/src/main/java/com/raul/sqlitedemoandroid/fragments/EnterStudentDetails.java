package com.raul.sqlitedemoandroid.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.raul.sqlitedemoandroid.R;
import com.raul.sqlitedemoandroid.database.DatabaseHelper;
import com.raul.sqlitedemoandroid.database.model.Student;

import java.util.ArrayList;
import java.util.List;

public class EnterStudentDetails extends Fragment {

    EditText studentName,studentAddress,studentRollNo,studentPhoneNumber;
    Button submit;
    private DatabaseHelper db;
    private List<Student> studentList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_enter_student_details, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {

        db=new DatabaseHelper(getContext());
        studentName=view.findViewById(R.id.studentName);
        studentAddress=view.findViewById(R.id.studentAddress);
        studentRollNo=view.findViewById(R.id.studentRollNo);
        studentPhoneNumber=view.findViewById(R.id.studentPhone);
        submit=view.findViewById(R.id.submit);

        submit.setOnClickListener(v ->
                createStudentDetails(studentName.getText().toString(),
                            studentAddress.getText().toString(),
                            studentPhoneNumber.getText().toString(),
                            studentRollNo.getText().toString()
                            ));

    }

    private void createStudentDetails(String stdName, String stdAddress, String stdPhone, String stdRoll) {

        long id=db.insertStudentRec(stdName,stdAddress,stdPhone,stdRoll);

        Student st=db.getStudent(id);

        if(st!=null) {
            studentList.add(0,st);
        }
        FragmentTransaction transaction2 = getActivity().getSupportFragmentManager().beginTransaction();
        transaction2.replace(R.id.fragDir, new StudentsInfo());
        transaction2.addToBackStack(null);
        transaction2.commit();
        Toast.makeText(getContext(),"Student Details Entered",Toast.LENGTH_LONG).show();

    }




}