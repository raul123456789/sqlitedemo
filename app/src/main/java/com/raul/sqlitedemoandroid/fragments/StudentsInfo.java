package com.raul.sqlitedemoandroid.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.raul.sqlitedemoandroid.R;
import com.raul.sqlitedemoandroid.database.DatabaseHelper;
import com.raul.sqlitedemoandroid.database.model.Student;
import com.raul.sqlitedemoandroid.utils.MyDividerItemDecoration;
import com.raul.sqlitedemoandroid.utils.RecyclerTouchListener;
import com.raul.sqlitedemoandroid.view.MainActivity;
import com.raul.sqlitedemoandroid.view.StudentsAdapter;

import java.util.ArrayList;
import java.util.List;

public class StudentsInfo extends Fragment {

    private DatabaseHelper db;
    private StudentsAdapter studentsAdapter;
    private List<Student> studentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView noNotesView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_students_info, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        db = new DatabaseHelper(getContext());
        studentList.addAll(db.getAllStudents());
        recyclerView = view.findViewById(R.id.studentREcyclerView);
        noNotesView = view.findViewById(R.id.empty_notes_view);
        studentsAdapter = new StudentsAdapter(getContext(), studentList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(studentsAdapter);
        toggleEmptyNotes();


        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this.getActivity(),
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {

            }

            @Override
            public void onLongClick(View view, int position) {
                showActionsDialog(position);
            }
        }));

    }

    private void showActionsDialog(int position) {

        CharSequence colors[] = new CharSequence[]{"Edit", "Delete"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choose option");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    showNoteDialog(true, studentList.get(position), position);
                } else {
                    deleteNote(position);
                }
            }
        });
        builder.show();

    }

    private void deleteNote(int position) {
        // deleting the note from db
        db.deleteNote(studentList.get(position));

        // removing the note from the list
        studentList.remove(position);
        studentsAdapter.notifyItemRemoved(position);

        toggleEmptyNotes();
    }

    private void showNoteDialog(boolean update, Student student, int position) {

        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(getContext());
        View view = layoutInflaterAndroid.inflate(R.layout.student_edit_dialog, null);

        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(getContext());
        alertDialogBuilderUserInput.setView(view);

        final EditText stdName = view.findViewById(R.id.studentNameEd);
        final EditText stdAddress = view.findViewById(R.id.studentAddressEd);
        final EditText stdPhone = view.findViewById(R.id.studentPhoneEd);
        final EditText stdRoll = view.findViewById(R.id.studentRollNoEd);

        if (update && student != null) {
            stdName.setText(student.getStudentName());
            stdAddress.setText(student.getStudentAddress());
            stdPhone.setText(student.getStudentPhone());
            stdRoll.setText(student.getStudentRoll());
        }

        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton(update ? "update" : "save", (dialogBox, id) -> {

                })
                .setNegativeButton("cancel",
                        (dialogBox, id) -> dialogBox.cancel());

        final AlertDialog alertDialog = alertDialogBuilderUserInput.create();
        alertDialog.show();

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {

            if (update && student != null) {
                updateStudentDetails(stdName.getText().toString(),stdAddress.getText().toString(),
                                    stdPhone.getText().toString(),stdRoll.getText().toString(),position);
            }

        });
    }

    private void updateStudentDetails(String stdName, String stdAddress, String stdPhone, String stdRoll, int position) {

        Student student=studentList.get(position);

        student.setStudentName(stdName);
        student.setStudentAddress(stdAddress);
        student.setStudentPhone(stdPhone);
        student.setStudentRoll(stdRoll);

        db.updateStudentRec(student);
        studentList.set(position,student);
        studentsAdapter.notifyDataSetChanged();
        Intent intent=new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        toggleEmptyNotes();

    }

    private void toggleEmptyNotes() {
        if (db.getStudentsCount() > 0) {
            noNotesView.setVisibility(View.GONE);
        } else {
            noNotesView.setVisibility(View.VISIBLE);
        }
    }


}