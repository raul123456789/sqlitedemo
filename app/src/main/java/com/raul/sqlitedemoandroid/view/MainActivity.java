package com.raul.sqlitedemoandroid.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.raul.sqlitedemoandroid.R;
import com.raul.sqlitedemoandroid.fragments.EnterStudentDetails;
import com.raul.sqlitedemoandroid.fragments.StudentsInfo;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    public Toolbar toolbar;
    public View headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        initializeDefaultFragment(savedInstanceState,0);
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbarM2S);
        toolbar.setTitle("     ");
        setSupportActionBar(toolbar);
        navigationView = findViewById(R.id.navigation_item_student);
        headerView = navigationView.getHeaderView(0);
        drawerLayout = findViewById(R.id.drawer_layout_Studentid);
        navigationView.setNavigationItemSelectedListener(this);
        toggleDrawer();
    }

    private void toggleDrawer() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,toolbar,
                R.string.navigation_drawer_close,
                R.string.navigation_drawer_open);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void closeDrawer() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    private void initializeDefaultFragment(Bundle savedInstanceState, int i) {
        if (savedInstanceState == null) {
            MenuItem menuItem = navigationView.getMenu().getItem(i).setChecked(true);
            onNavigationItemSelected(menuItem);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.nav_studentinfo:
                changeFrag(new StudentsInfo());
                closeDrawer();
                toolbar.setTitle(R.string.studntinfo);
                break;
            case R.id.nav_enter_student:
                changeFrag(new EnterStudentDetails());
                closeDrawer();
                toolbar.setTitle(R.string.enter_detials);
                break;
        }
        return true;
    }

    private void changeFrag(Fragment fragment) {
        FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
        transaction2.replace(R.id.fragDir, fragment);
        transaction2.addToBackStack(null);
        transaction2.commit();
    }
}